package Search.SherlockandArray;

import java.util.Scanner;

public class Solution {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int T = Integer.parseInt(in.nextLine());
		String[] ip;

		String[] result = new String[T];
		int j = 0;

		while (T-- > 0) {
			int N = Integer.parseInt(in.nextLine());
			ip = in.nextLine().split("\\s+");

			int[] prefixSum = new int[N];

			prefixSum[0] = Integer.parseInt(ip[0]);
			for (int i = 1; i < N; i++) {
				prefixSum[i] = prefixSum[i - 1] + Integer.parseInt(ip[i]);
			}

			String answer = "NO";

			for (int i = 1; i < N; i++) {
				int leftSum = prefixSum[i - 1];
				int rightSum = prefixSum[N - 1] - prefixSum[i];

				if (leftSum == rightSum) {
					answer = "YES";
					break;
				}
			}

			if (N == 1)
				answer = "YES";

			result[j++] = answer;
		}

		in.close();

		for (String x : result)
			System.out.println(x);
	}

}
