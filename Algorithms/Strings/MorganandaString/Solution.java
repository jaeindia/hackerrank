package Strings.MorganandaString;

import java.util.Arrays;
import java.util.Scanner;

// TO DO: https://github.com/tyuan73/MyAlg/blob/master/HackerRank/src/MorganAndString.java
// Last test case

public class Solution {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);

		int T = Integer.parseInt(in.nextLine());
		String[] result = new String[T];
		int ctr = 0;

		while (T-- > 0) {
			char[] jackChars = in.nextLine().trim().toCharArray();
			char[] danielChars = in.nextLine().trim().toCharArray();

			int jackLength = jackChars.length;
			int danielLength = danielChars.length;

			int i = 0;
			int j = 0;

			StringBuilder minimalStr = new StringBuilder();

			while ((i < jackLength) || (j < danielLength)) {
				if ((i < jackLength) && (j < danielLength)) {
//					System.out.println("i " + i + " j " + j);
					if (jackChars[i] < danielChars[j])
						minimalStr.append(jackChars[i++]);
					else if (jackChars[i] > danielChars[j])
						minimalStr.append(danielChars[j++]);
					else {
						int x = i, y = j;
						boolean flag = false;
						for (; x < jackLength & y < danielLength; x++, y++) {
							if (jackChars[x] < danielChars[y]) {
								minimalStr.append(jackChars[i++]);
								flag = true;
								break;
							} else if (jackChars[x] > danielChars[y]) {
								minimalStr.append(danielChars[j++]);
								flag = true;
								break;
							}
						}

						if (!flag) {
							minimalStr.append(Arrays.copyOfRange(jackChars, i, x));
							minimalStr.append(Arrays.copyOfRange(danielChars, j, y));
							break;
						}
					}
				} else if (i == jackLength) {
					minimalStr.append(Arrays.copyOfRange(danielChars, j, danielLength));
					break;
				} else {
					minimalStr.append(Arrays.copyOfRange(jackChars, i, jackLength));
					break;
				}
			}

			result[ctr++] = minimalStr.toString();
		}

		for (String x : result)
			System.out.println(x);

		in.close();
	}

}
