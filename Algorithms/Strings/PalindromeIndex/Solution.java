package Strings.PalindromeIndex;

import java.util.Scanner;

public class Solution {

	static boolean isPalindrome(char[] str, int start, int end) {
		while (start <= end) {
			if (str[start] != str[end])
				return false;

			start++;
			end--;
		}

		return true;
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);

		int T = Integer.parseInt(in.nextLine().trim());
		int[] result = new int[T];
		int j = 0;

		while (T-- > 0) {
			char[] str = in.nextLine().trim().toCharArray();
			int start = 0;
			int end = str.length - 1;

			result[j] = -1;

			while (start <= end) {
				if (str[start] != str[end]) {
					if ((str[start + 1] == str[end]) && isPalindrome(str, start + 2, end - 1))
						result[j] = start;
					else if ((str[start] == str[end - 1]) && isPalindrome(str, start + 1, end - 2))
						result[j] = end;
					
					break;
				}

				start++;
				end--;
			}

			j++;
		}

		in.close();

		for (int x : result)
			System.out.println(x);
	}
}
