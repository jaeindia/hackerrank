package Strings.MakeitAnagram;

import java.util.Scanner;

public class Solution {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);

		int[] count = new int[26];
		int deletionCount = 0;

		for (char x : in.nextLine().toCharArray())
			count[x - 97]++;

		for (char x : in.nextLine().toCharArray())
			count[x - 97]--;

		for (int x : count)
			deletionCount += Math.abs(x);

		System.out.println(deletionCount);
		
		in.close();
	}
}
