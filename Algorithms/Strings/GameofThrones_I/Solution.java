package Strings.GameofThrones_I;

import java.util.Scanner;

public class Solution {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);

		char[] str;
		int[] alphabet = new int[26];

		str = in.nextLine().toCharArray();
		int palindromeFlag = 0;

		for (int i = 0; i < str.length; i++) {
			int pos = str[i] - 97;
			alphabet[pos]++;
		}

		for (int x : alphabet) {
			if ((x & 1) == 1)
				palindromeFlag++;
		}

		if (palindromeFlag > 1)
			System.out.println("NO");
		else
			System.out.println("YES");

	}
}
