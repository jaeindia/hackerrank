package Strings.AlternatingCharacters;

import java.util.Scanner;

public class Solution {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int T = Integer.parseInt(in.nextLine());

		char[] str;
		int delCounter = 0;

		int[] result = new int[T];
		int j = 0;

		while (T-- > 0) {
			str = in.nextLine().toCharArray();

			for (int i = 0; i < str.length - 1; i++) {
				if (str[i] == str[i + 1])
					delCounter++;
			}

			result[j++] = delCounter;
			delCounter = 0;
		}

		for (int x : result)
			System.out.println(x);

	}
}
