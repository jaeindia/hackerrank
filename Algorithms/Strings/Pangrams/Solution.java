package Strings.Pangrams;

import java.util.HashSet;
import java.util.Scanner;

public class Solution {
	public static void main(String[] args) {
		HashSet<Character> alphabet = new HashSet<Character>();
		Scanner in = new Scanner(System.in);
		
		String s = in.nextLine();
		
		for (char x : s.toLowerCase().toCharArray()) {
			if ((int) x >= 97 && (int) x <= 122)
				alphabet.add(x);
		}
		
		if (alphabet.size() == 26)
			System.out.println("pangram");
		else
			System.out.println("not pangram");
	}
}
