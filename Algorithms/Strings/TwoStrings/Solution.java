package Strings.TwoStrings;

import java.util.Scanner;

public class Solution {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int T = Integer.parseInt(in.nextLine());

		while (T-- > 0) {
			int[] alphabet = new int[26];

			boolean flag = false;

			String A = in.nextLine();
			String B = in.nextLine();

			for (char x : A.toCharArray())
				alphabet[x - 97]++;

			for (char x : B.toCharArray())
				if (alphabet[x - 97] != 0)
					flag = true;

			if (flag)
				System.out.println("YES");
			else
				System.out.println("NO");
		}

		in.close();
	}

}
