package Strings.FunnyString;

import java.util.Scanner;

public class Solution {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int strDiff = 0;
		int revDiff = 0;
		boolean flag = false;

		int T = Integer.parseInt(in.nextLine());
		String result = "";
		String lineBreak = "\n";

		for (int i = 0; i < T; i++) {
			String ipString = in.nextLine();
			char[] str = ipString.toCharArray();
			char[] rev = new StringBuffer(ipString).reverse().toString().toCharArray();
			flag = true;

			if (i + 1 == T)
				lineBreak = "";

			for (int j = 1; j < ipString.length(); j++) {
				strDiff = Math.abs(str[j] - str[j - 1]);
				revDiff = Math.abs(rev[j] - rev[j - 1]);

				if (strDiff == revDiff)
					continue;
				else
					flag = false;
			}

			if (flag)
				result += "Funny" + lineBreak;
			else
				result += "Not Funny" + lineBreak;
		}

		System.out.println(result);
	}
}
