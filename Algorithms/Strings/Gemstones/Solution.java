package Strings.Gemstones;

import java.util.Scanner;

public class Solution {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);

		int N = Integer.parseInt(in.nextLine());
		int[][] alphabet = new int[N][26];

		for (int i = 0; i < N; i++) {
			String elements = in.nextLine();

			for (char x : elements.toCharArray())
				alphabet[i][x - 97]++;
		}

		int counter = 0;

		for (int j = 0; j < 26; j++) {
			boolean flag = true;
			for (int i = 0; i < N; i++) {
				if (alphabet[i][j] == 0) {
					flag = false;
					break;
				}
			}

			if (flag)
				counter++;
		}

		System.out.println(counter);
	}
}
