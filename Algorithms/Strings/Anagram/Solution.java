package Strings.Anagram;

import java.util.Scanner;

public class Solution {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);

		int T = Integer.parseInt(in.nextLine());

		while (T-- > 0) {
			String str = in.nextLine();
			int minChange = 0;
			int strLength = str.length();
			int[] alphabetArr = new int[26];

			if (strLength % 2 == 0) {
				String S1 = str.substring(0, strLength/2);
				String S2 = str.substring(strLength/2);

				for (int i = 0; i < S1.length(); i++)
					alphabetArr[S1.charAt(i) - 97]++;

				for (int i = 0; i < S2.length(); i++) {
					if (alphabetArr[S2.charAt(i) - 97] == 0)
						minChange++;
					else
						alphabetArr[S2.charAt(i) - 97]--;
				}

			} else
				minChange = -1;

			System.out.println(minChange);
		}

		in.close();
	}
}
