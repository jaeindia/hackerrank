package Sorting.IntrotoTutorialChallenges;

import java.util.Scanner;

public class Solution {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);

		int V = Integer.parseInt(in.nextLine());
		int n = Integer.parseInt(in.nextLine());
		String[] arString = in.nextLine().split("\\s+");

		int index = 0;

		for (String x : arString) {
			if (Integer.parseInt(x) == V)
				break;

			index++;
		}

		System.out.println(index);
	}
}
