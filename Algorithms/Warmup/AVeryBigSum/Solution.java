package Warmup.AVeryBigSum;

import java.util.Scanner;

public class Solution {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		int N = Integer.parseInt(in.nextLine());
		long sum = 0;
		
		for (String number : in.nextLine().split("\\s+")) {
			sum += Integer.parseInt(number);
		}

		System.out.println(sum);

	}
}
