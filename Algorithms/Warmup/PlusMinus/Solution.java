package Warmup.PlusMinus;

import java.util.Scanner;

public class Solution {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int N = Integer.parseInt(in.nextLine());
		int pos = 0;
		int neg = 0;
		int zero = 0;
		
		for (String element : in.nextLine().split("\\s+")) {
			if (Integer.parseInt(element) > 0)
				pos++;
			else if (Integer.parseInt(element) == 0)
				zero++;
			else
				neg++;
		}
		
		System.out.format("%.6f\n%.6f\n%.6f", (pos / (double) N), (neg / (double) N), (zero / (double) N));

	}
}
