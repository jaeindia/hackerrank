package Warmup.DiagonalDifference;

import java.util.Scanner;

public class Solution {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int N = Integer.parseInt(in.nextLine());
		int diff = 0;
		int diag1 = 0;
		int diag2 = N - 1;
		int ctr = 0;

		for (int i = 0; i < N; i++) {
			for (String element : in.nextLine().split("\\s+")) {
				if (diag1 == ctr)
					diff += Integer.parseInt(element);

				if (diag2 == ctr)
					diff -= Integer.parseInt(element);

				ctr++;
			}

			diag1++;
			diag2--;

			ctr = 0;
		}

		System.out.println(Math.abs(diff));
	}
}
