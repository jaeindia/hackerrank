package Warmup.UtopianTree;

import java.util.Scanner;

public class Solution {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		int T = in.nextInt();
		int[] N = new int[T];
		int cycles = 0;
		int k = 0;
		
		for (int i = 0; i < T; i++) {
			cycles = in.nextInt();
			
			N[k] = 1;
			
			if (cycles != 0) {
				for (int j = 1 ; j <= cycles; j++) {
					if (j%2 != 0)
						N[k] *= 2;
					else
						N[k] += 1;					
				}
			}
			
			k++;
		}
		
		for (int x : N)
			System.out.println(x);

	}

}
