package DynamicProgramming.FibonacciModified;

import java.math.BigInteger;
import java.util.Scanner;

public class Solution {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);

		BigInteger a = new BigInteger(in.nextInt() + "");
		BigInteger b = new BigInteger(in.nextInt() + "");
		BigInteger c = new BigInteger(0 + "");
		int N = in.nextInt();

		for (int i = 3; i <= N; i++) {
			c = b.multiply(b);
			c = c.add(a);

			a = b;
			b = c;
		}

		System.out.println(c);
		in.close();
	}

}
