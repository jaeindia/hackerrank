package Implementation.ACMICPCTeam;

import java.util.Scanner;

public class Solution {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);

		String arg[] = in.nextLine().split("\\s+");

		int N = Integer.parseInt(arg[0]);
		int M = Integer.parseInt(arg[1]);

		String topic[] = new String[M];

		for (int i = 0; i < N; i++)
			topic[i] = in.nextLine();

		int max = Integer.MIN_VALUE;
		int maxTeamCount = 0;

		for (int i = 0; i < N; i++) {
			for (int j = i + 1; j < N; j++) {
				int tempCount = 0;
				for (int k = 0; k < M; k++) {
					if (((topic[i].charAt(k) - 48) | (topic[j].charAt(k) - 48)) == 1)
						tempCount++;
				}

				if (max < tempCount) {
					max = tempCount;
					maxTeamCount = 1;
				} else if (max == tempCount)
					maxTeamCount++;
			}
		}

		System.out.println(max + "\n" + maxTeamCount);
	}
}
