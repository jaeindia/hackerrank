package Implementation.CavityMap;

import java.util.Scanner;

public class Solution {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = Integer.parseInt(in.nextLine());

		char[][] map = new char[n][n];

		for (int i = 0; i < n; i++) {
			int j = 0;
			
			for (char x : in.nextLine().toCharArray())
				map[i][j++] = x;
		}

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0 || j == 0 || i == n - 1 || j == n - 1)
					System.out.print(map[i][j]);

				else if (map[i][j] > map[i][j - 1] && map[i][j] > map[i][j + 1] && map[i][j] > map[i - 1][j]
						&& map[i][j] > map[i + 1][j]) {
					System.out.print('X');
				}

				else
					System.out.print(map[i][j]);
			}

			System.out.println();
		}
	}

}
