package Implementation.TaumandBday;

import java.util.Scanner;

public class Solution {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);

		int T = Integer.parseInt(in.nextLine());

		StringBuffer result = new StringBuffer();
		String lf = "\n";

		while (T-- > 0) {
			String ip[] = new String[2];
			ip = in.nextLine().split("\\s+");

			long B = Integer.parseInt(ip[0]);
			long W = Integer.parseInt(ip[1]);

			ip = new String[3];
			ip = in.nextLine().split("\\s+");

			long X = Integer.parseInt(ip[0]);
			long Y = Integer.parseInt(ip[1]);
			long Z = Integer.parseInt(ip[2]);

			long price = 0;

			if (X > Y + Z)
				price = B * (Y + Z) + W * Y;
			else if (Y > X + Z)
				price = B * X + W * (X + Z);
			else
				price = B * X + W * Y;

			if (T == 0)
				lf = "";

			result.append(price + lf);
		}

		in.close();

		System.out.println(result);
	}
}
