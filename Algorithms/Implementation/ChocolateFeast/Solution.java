package Implementation.ChocolateFeast;

import java.util.Scanner;

public class Solution {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);

		int T = Integer.parseInt(in.nextLine());

		while (T-- > 0) {
			String[] ip = new String[3];
			ip = in.nextLine().split("\\s+");

			int N = Integer.parseInt(ip[0]);
			int C = Integer.parseInt(ip[1]);
			int M = Integer.parseInt(ip[2]);

			int buy = N / C;
			int count = buy;

			while (buy >= M) {
				int buyd = buy / M;
				count += buyd;

				int buyr = buy % M;

				buy = buyd + buyr;
			}

			System.out.println(count);
		}
	}
}
