package Implementation.Cutthesticks;

import java.util.Scanner;

public class Solution {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);

		int N = Integer.parseInt(in.nextLine());
		int[] a = new int[N];
		int min = Integer.MAX_VALUE;

		for (int i = 0; i < N; i++) {
			a[i] = in.nextInt();
			min = Math.min(min, a[i]);
		}

		int counter = -1;

		while (counter != 0) {
			int tempMin = Integer.MAX_VALUE;
			counter = 0;

			for (int i = 0; i < N; i++) {
				if (a[i] != 0) {
					a[i] -= min;

					if (a[i] != 0)
						tempMin = Math.min(tempMin, a[i]);

					counter++;
				}
			}

			min = tempMin;

			if (counter > 0)
				System.out.println(counter);
		}
	}

}
