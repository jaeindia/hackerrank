package Implementation.ModifiedKaprekarNumbers;

import java.util.Scanner;

public class Solution {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);

		int p = in.nextInt();
		int q = in.nextInt();

		boolean invalidRange = true;

		for (int i = p; i <= q; i++) {
			long sqr = (long) Math.pow(i, 2);
			int length = String.valueOf(i).length();
			long divisor = (long) Math.pow(10, length);
			long l = sqr / divisor;
			long r = sqr % divisor;

			if (l + r == i) {
				System.out.print(i + " ");
				invalidRange = false;
			}
		}

		if (invalidRange)
			System.out.println("INVALID RANGE");
	}

}
