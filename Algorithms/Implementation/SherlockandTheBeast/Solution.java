package Implementation.SherlockandTheBeast;

import java.util.Scanner;

public class Solution {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int T = Integer.parseInt(in.nextLine());
		StringBuilder res = new StringBuilder();

		while (T-- > 0) {
			int N = Integer.parseInt(in.nextLine());
			int length = N;

			while (N % 3 != 0) {
				N -= 5;
			}

			if (N >= 0) {
				for (int i = 0; i < N; i++) {
					res.append("5");
				}

				for (int i = 0; i < (length - N); i++) {
					res.append("3");
				}
			} else {
				res.append("-1");
			}

			res.append("\n");
		}

		System.out.println(res);

		in.close();
	}

}
