package Implementation.TheTimeinWords;

import java.util.Scanner;

public class Solution {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String timeInWord[] = { "", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten",
				"eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen",
				"twenty", "twenty one", "twenty two", "twenty three", "twenty four", "twenty five", "twenty six",
				"twenty seven", "twenty eight", "twenty nine" };

		int H = in.nextInt();
		int M = in.nextInt();

		String time = "";

		if (M == 0)
			time = timeInWord[H] + " o' clock";
		else if (M == 1)
			time = timeInWord[M] + " minute past " + timeInWord[H];
		else if (M == 15)
			time = "quarter past " + timeInWord[H];
		else if (M == 30)
			time = "half past " + timeInWord[H];
		else if (M == 45)
			time = "quarter to " + timeInWord[H + 1];
		else if (M < 30)
			time = timeInWord[M] + " minutes past " + timeInWord[H];
		else
			time = timeInWord[60 - M] + " minutes to " + timeInWord[H + 1];

		System.out.println(time);
	}
}
