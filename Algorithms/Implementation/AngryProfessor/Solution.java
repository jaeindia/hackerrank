package Implementation.AngryProfessor;

import java.util.Scanner;

public class Solution {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);

		int T = Integer.parseInt(in.nextLine());

		StringBuilder result = new StringBuilder();
		String lf = "\n";

		while (T-- > 0) {
			String ip[] = new String[2];
			ip = in.nextLine().split("\\s+");

			int N = Integer.parseInt(ip[0]);
			int K = Integer.parseInt(ip[1]);

			ip = new String[N];
			ip = in.nextLine().split("\\s+");

			int beforeClassStart = 0;

			for (String x : ip) {
				if (Integer.parseInt(x) <= 0)
					beforeClassStart++;
			}

			if (T == 0)
				lf = "";

			if (beforeClassStart >= K)
				result.append("NO" + lf);
			else
				result.append("YES" + lf);
		}

		in.close();

		System.out.println(result);
	}
}
