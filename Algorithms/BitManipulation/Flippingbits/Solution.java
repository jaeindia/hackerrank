package BitManipulation.Flippingbits;

import java.util.Scanner;

public class Solution {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		long xorVal = (long) (Math.pow(2, 32) - 1);

		int T = Integer.parseInt(in.nextLine());
		long[] result = new long[T];
		int i = 0;

		while (T-- > 0) {
			long n = Long.parseLong(in.nextLine());
			result[i++] = n ^ xorVal;
		}

		for (long x : result)
			System.out.println(x);
	}
}
