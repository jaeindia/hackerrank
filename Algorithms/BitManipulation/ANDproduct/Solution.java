package BitManipulation.ANDproduct;

import java.util.Scanner;

public class Solution {

	public static long andProduct(long A, long B) {
		int ceilB = (int) (Math.log(B) / Math.log(2.0));
		int floorA = (int) (Math.log(A) / Math.log(2.0));

		if (ceilB >= floorA + 1)
			return 0;
		
		for (long i = A; i <= B; i++)
			A &= i;

		return A;
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);

		int T = Integer.parseInt(in.nextLine());

		while (T-- > 0) {
			String ip[] = in.nextLine().split("\\s+");
			long A = Long.parseLong(ip[0]);
			long B = Long.parseLong(ip[1]);

			System.out.println(andProduct(A, B));
		}

		in.close();
	}
}
