package Euler_1_Multiplesof3and5;

import java.math.BigInteger;
import java.util.Scanner;

public class Solution {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int T = in.nextInt();
		int N = 0;

		BigInteger sum;
		int q = 0;

		String result[] = new String[T];
		int i = 0;

		while (T-- > 0) {
			N = in.nextInt() - 1;

			// Multiples of 3
			q = N / 3;
			sum = ((new BigInteger(q + "").multiply(new BigInteger(q + 1 + ""))).divide(new BigInteger(2 + ""))
					.multiply(new BigInteger(3 + "")));

			// Multiples of 5
			q = N / 5;
			sum = sum.add((new BigInteger(q + "").multiply(new BigInteger(q + 1 + ""))).divide(new BigInteger(2 + ""))
					.multiply(new BigInteger(5 + "")));

			// Multiples of 15
			q = N / 15;
			sum = sum.subtract((new BigInteger(q + "").multiply(new BigInteger(q + 1 + "")))
					.divide(new BigInteger(2 + "")).multiply(new BigInteger(15 + "")));

			result[i++] = sum.toString();
		}

		for (String x : result)
			System.out.println(x);
	}
}
