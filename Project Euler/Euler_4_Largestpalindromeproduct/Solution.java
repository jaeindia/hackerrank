package Euler_4_Largestpalindromeproduct;

import java.util.Scanner;

public class Solution {
	public static boolean isPalindrome(int N) {
		if ((N + "").equals(new StringBuffer(N + "").reverse().toString()))
			return true;

		return false;
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int T = in.nextInt();
		int N = 0;
		int product = 0;
		int maxProduct = 0;
		int k = 0;

		int result[] = new int[T];

		while (T-- > 0) {
			N = in.nextInt();

			for (int i = 100; i <= 999; i++) {
				for (int j = 100; j <= i; j++) {
					product = i * j;
					if (product <= N && isPalindrome(product)) {
						maxProduct = Math.max(product, maxProduct);
					}
				}
			}
			result[k++] = maxProduct;
			maxProduct = 0;
		}

		for (int x : result)
			System.out.println(x);
	}

}
