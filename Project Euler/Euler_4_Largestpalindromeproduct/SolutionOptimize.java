package Euler_4_Largestpalindromeproduct;

import java.util.ArrayList;
import java.util.Scanner;

public class SolutionOptimize {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int T = in.nextInt();
		int N = 0;
		int index = 0;
		boolean stopFlag = false;

		ArrayList<Integer> palindrome = new ArrayList<Integer>();

		int result[] = new int[T];

		for (int i = 9; i >= 1; i--) {
			for (int j = 9; j >= 0; j--) {
				for (int k = 9; k >= 0; k--) {
					palindrome.add(Integer.parseInt(("" + i + j + k + k + j + i)));
				}
			}
		}

		while (T-- > 0) {
			N = in.nextInt();
			stopFlag = false;

			for (int x : palindrome) {
				if (x < N && !stopFlag) {
					for (int i = 100; i <= 999; i++) {
						if ((x % i == 0) && (x / i <= 999)) {
							result[index++] = x;
							stopFlag = true;
							break;
						}
					}
				}
			}
		}

		for (int x : result)
			System.out.println(x);
	}

}
