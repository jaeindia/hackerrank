package Euler_2_EvenFibonaccinumbers;

import java.math.BigInteger;
import java.util.Scanner;

public class Solution {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int T = in.nextInt();
		long N = 0;
		long a = 1;
		long b = 2;

		BigInteger sum = new BigInteger(0 + "");
		String result[] = new String[T];
		int i = 0;

		while (T-- > 0) {
			N = in.nextLong();
			while (b <= N) {
				if ((b & 1) == 0)
					sum = sum.add(new BigInteger(b + ""));

				b = a + b;
				a = b - a;
			}

			result[i++] = sum.toString();
			a = 1;
			b = 2;
			sum = new BigInteger(0 + "");
		}

		for (String x : result)
			System.out.println(x);
	}
}
