package Euler_3_Largestprimefactor;

import java.util.Scanner;

public class Solution {
	public static boolean isPrime(long N) {
		int limit = (int) Math.sqrt(N);
		
		for (int i = 3; i <= limit; i += 2) {
			if (N % i == 0)
				return false;
		}

		return true;
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int T = in.nextInt();
		long N = 0;
		long largestPrime = 0;
		int sqrt = 0;
		int j = 0;

		String result[] = new String[T];

		while (T-- > 0) {
			N = in.nextLong();

			if ((N & 1) == 0) {
				while (N % 2 == 0) {
					N /= 2;
				}
				largestPrime = 2;
			}

			sqrt = (int) Math.sqrt(N);

			for (int i = 3; i <= sqrt; i += 2) {
				while (N % i == 0) {
					N /= i;
					largestPrime = i;
				}
			}
			
			if (N > largestPrime && isPrime(N))
				largestPrime = N;

			result[j++] = largestPrime + "";
			largestPrime = 0;
		}

		for (String x : result)
			System.out.println(x);

	}
}
