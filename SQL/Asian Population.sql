--Asian Population

SELECT sum(City.Population) Result
FROM City,
	Country
WHERE City.CountryCode = Country.Code
	AND Country.Continent = 'Asia';
