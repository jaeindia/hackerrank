--Population Density Difference

SELECT MAX(Population) - MIN(Population) Result
FROM City;