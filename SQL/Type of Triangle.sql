--Type of Triangle

SELECT CASE 
		WHEN sum1 > C
			AND sum2 > A
			AND sum3 > B
			THEN CASE 
					WHEN A = B
						AND B = C
						THEN 'Equilateral'
					WHEN A = B
						OR B = C
						OR A = C
						THEN 'Isosceles'
					ELSE 'Scalene'
					END
		ELSE 'Not A Triangle'
		END Result
FROM (
	SELECT A,
		B,
		C,
		A + B sum1,
		B + C sum2,
		C + A sum3
	FROM Triangles
	);
