--The Report

SELECT CASE 
		WHEN Grades.Grade < 8
			THEN 'NULL'
		ELSE Students.Name
		END Name,
	Grades.Grade,
	Students.Marks
FROM Students
INNER JOIN Grades
	ON Students.Marks BETWEEN Grades.min_mark
			AND Grades.max_mark
ORDER BY Grades.Grade DESC,
	Students.Name ASC;