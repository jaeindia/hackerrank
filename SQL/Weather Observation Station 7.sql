--Weather Observation Station 7

SELECT DISTINCT CITY
FROM STATION
WHERE SUBSTR(lower(CITY), length(CITY), 1) IN (
		'a',
		'e',
		'i',
		'o',
		'u'
		);
