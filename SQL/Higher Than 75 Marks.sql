--Higher Than 75 Marks

SELECT Name
FROM Students
WHERE Marks > 75
ORDER BY SUBSTR(LOWER(Name), length(Name) - 2, 3),
	ID;

