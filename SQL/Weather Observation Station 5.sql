--Weather Observation Station 5

(
		SELECT *
		FROM (
			SELECT CITY,
				length(CITY) len
			FROM STATION
			ORDER BY len,
				CITY
			)
		WHERE rownum = 1
		)
UNION
(
	SELECT *
	FROM (
		SELECT CITY,
			length(CITY) len
		FROM STATION
		ORDER BY len DESC,
			CITY ASC
		)
	WHERE rownum = 1
	);
