--Average Population of Each Continent

SELECT Country.Continent,
	FLOOR(AVG(City.Population)) AvgCityPopulation
FROM City,
	Country
WHERE Country.Code = City.CountryCode
GROUP BY Country.Continent;
