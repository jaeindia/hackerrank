--Weather Observation Station 9

SELECT DISTINCT CITY
FROM STATION
WHERE SUBSTR(lower(CITY), 1, 1) NOT IN (
		'a',
		'e',
		'i',
		'o',
		'u'
		);

