--Weather Observation Station 11

SELECT DISTINCT CITY
FROM STATION
WHERE SUBSTR(lower(CITY), 1, 1) NOT IN (
		'a',
		'e',
		'i',
		'o',
		'u'
		)
	OR SUBSTR(lower(CITY), length(CITY), 1) NOT IN (
		'a',
		'e',
		'i',
		'o',
		'u'
		);
