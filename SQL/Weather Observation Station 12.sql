--Weather Observation Station 12

SELECT DISTINCT CITY
FROM STATION
WHERE SUBSTR(lower(CITY), 1, 1) NOT IN (
		'a',
		'e',
		'i',
		'o',
		'u'
		)
	AND SUBSTR(lower(CITY), length(CITY), 1) NOT IN (
		'a',
		'e',
		'i',
		'o',
		'u'
		);