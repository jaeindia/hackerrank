--The PADS

SELECT subQuery2.result
FROM (
	(
		SELECT Name || '(' || SUBSTR(Occupation, 1, 1) || ')' result,
			Name,
			1 ordering
		FROM Occupations
		)
	UNION
	(
		SELECT 'There are total' || ' ' || subQuery.count || ' ' || LOWER(subQuery.Occupation) || 's.' result,
			subQuery.Occupation Name,
			subQuery.count ordering
		FROM (
			SELECT Occupation,
				COUNT(*) count
			FROM Occupations
			GROUP BY Occupation
			) subQuery
		)
	) subQuery2
ORDER BY subQuery2.ordering,
	subQuery2.Name;