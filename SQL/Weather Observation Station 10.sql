--Weather Observation Station 10

SELECT DISTINCT CITY
FROM STATION
WHERE SUBSTR(lower(CITY), length(CITY), 1) NOT IN (
		'a',
		'e',
		'i',
		'o',
		'u'
		);
