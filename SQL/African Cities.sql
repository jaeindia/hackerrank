--African Cities

SELECT City.Name Result
FROM City,
	Country
WHERE City.CountryCode = Country.Code
	AND Country.Continent = 'Africa';
